"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var placeholder = document.createElement("li");
placeholder.className = "placeholder";
var ToDoBanner = React.createClass({
	render: function render() {
		return React.createElement(
			"h3",
			null,
			"Tasks"
		);
	}
});

var ToDoList = React.createClass({
	Remove: function Remove(e) {
		this.props.onDelete(e);
	},
	DragStart: function DragStart(e) {
		this.dragged = e.currentTarget;
		e.dataTransfer.effectAllowed = 'move';
	},
	DragEnd: function DragEnd(e) {
		this.dragged.style.display = "";
		var IshasNode = false;

		Array.prototype.forEach.call(this.dragged.parentNode.childNodes, function (node) {
			if (node.className == "placeholder") IshasNode = true;
		});
		if (!IshasNode) return;
		this.dragged.parentNode.removeChild(placeholder);
		var data = this.props.items;
		var from = Number(this.dragged.dataset.id);
		var to = Number(this.over.dataset.id);
		if (from < to) to--;
		if (this.nodePlacement == "after") to++;
		data.splice(to, 0, data.splice(from, 1)[0]);
		this.setState({ data: data });
	},
	DragOver: function DragOver(e) {

		e.preventDefault();
		this.dragged.style.display = "none";

		if (e.target.className == "placeholder") return;
		this.over = e.target;
		// Inside the dragOver method
		var relY = e.clientY - this.over.offsetTop;
		var height = this.over.offsetHeight / 2;
		var parent = e.target.parentNode;

		if (relY > height) {
			this.nodePlacement = "after";
			parent.insertBefore(placeholder, e.target.nextElementSibling);
		} else if (relY < height) {
			this.nodePlacement = "before";
			parent.insertBefore(placeholder, e.target);
		}
	},
	render: function render() {

		var createItem = function createItem(itemText, i) {

			return React.createElement(
				ToDoListItem,
				{ key: i, value: i, onDragEnd: this.DragEnd,
					onDragStart: this.DragStart, onRemove: this.Remove },
				itemText
			);
		};
		var allitems = this.props.items;
		// Here is the filter function 
		var status = this.props.filter[0].Status;
		switch (status) {
			case 'false':
				allitems = allitems.filter(function (t) {
					return !t.isDone;
				});
				break;
			case 'true':
				allitems = allitems.filter(function (t) {
					return t.isDone;
				});
		};
		// Here is the search function 
		var queryText = this.props.filter[0].keyword;

		if (queryText) {
			var queryResult = [];
			allitems.forEach(function (item) {
				if (item.item.toLowerCase().indexOf(queryText) != -1) queryResult.push(item);
			});
			return React.createElement(
				"ul",
				{ onDragOver: this.DragOver },
				queryResult.map(createItem, this)
			);
		}

		return React.createElement(
			"ul",
			{ onDragOver: this.DragOver },
			allitems.map(createItem, this)
		);
	}
});

var ToDoListItem = React.createClass({
	ChangeHandler: function ChangeHandler(e) {
		this.setState({
			value: e.target.checked
		});
		this.props.children.isDone = e.target.checked;
	},
	RemoveHandler: function RemoveHandler() {
		this.props.onRemove(this.props.value);
	},
	DragEndHandler: function DragEndHandler(e) {
		this.props.onDragEnd(e);
	},
	DragStartHandler: function DragStartHandler(e) {
		this.props.onDragStart(e);
	},
	render: function render() {

		var classCompleted = "taskItem completed";
		if (!this.props.children.isDone) classCompleted = "taskItem";
		return React.createElement(
			"li",
			{ className: classCompleted, "data-id": this.props.value,
				key: this.props.value, draggable: "true", onDragEnd: this.DragEndHandler,
				onDragStart: this.DragStartHandler },
			React.createElement(
				"button",
				{ type: "button", className: "close pull-right", "aria-hidden": "true", onClick: this.RemoveHandler },
				"\xD7"
			),
			React.createElement("input", { type: "checkbox", className: "checkboxCompleted", onChange: this.ChangeHandler, defaultChecked: this.props.children.isDone }),
			React.createElement(
				"span",
				null,
				this.props.children.item
			)
		);
	}
});

var Button = function (_React$Component) {
	_inherits(Button, _React$Component);

	function Button(props) {
		_classCallCheck(this, Button);

		var _this = _possibleConstructorReturn(this, (Button.__proto__ || Object.getPrototypeOf(Button)).call(this, props));

		_this.state = { active: false };
		return _this;
	}

	_createClass(Button, [{
		key: "click",
		value: function click() {
			this.setState({ active: true });
		}
	}, {
		key: "render",
		value: function render() {
			var classes = classnames('specialbutton', { active: this.state.active });
			return React.createElement(
				"button",
				{ className: classes, onClick: this.click.bind(this) },
				"Click me"
			);
		}
	}]);

	return Button;
}(React.Component);

var ToDoForm = React.createClass({
	getInitialState: function getInitialState() {
		return { item: '' };
	},
	handleSubmit: function handleSubmit(e) {
		e.preventDefault();
		this.props.onFormSubmit(this.state.item);
		this.setState({ item: '' });
		ReactDOM.findDOMNode(this.refs.item).focus();
		return;
	},
	onChange: function onChange(e) {
		this.setState({
			item: e.target.value
		});
	},
	render: function render() {
		return React.createElement(
			"div",
			{ className: "row" },
			React.createElement(
				"form",
				{ onSubmit: this.handleSubmit },
				React.createElement(
					"div",
					{ className: "form-group addTaskBox col-sm-12" },
					React.createElement("input", { type: "text", placeholder: "Add new task.", className: "todoField form-control", ref: "item", onChange: this.onChange, value: this.state.item }),
					React.createElement("input", { type: "submit", className: "btn btn-default", style: { "float": "left", "marginLeft": "5px" }, value: "Add" })
				)
			)
		);
	}
});

var ToDoFilter = React.createClass({
	isActive: function isActive(value) {
		return 'btn ' + (value === this.props.filter[0].Status ? 'btn-primary' : 'default');
	},
	render: function render() {
		var onFilter1 = this.props.onFilter;
		var onSearch1 = this.props.onSearch;
		return React.createElement(
			"div",
			{ className: "row" },
			React.createElement(
				"div",
				{ className: "col-xs-12" },
				React.createElement(
					"div",
					{ id: "todo-filter", className: "input-group" },
					React.createElement(
						"span",
						{ className: "input-group-btn" },
						React.createElement(
							"button",
							{ className: "btnSearchTask btn btn-default", type: "button" },
							React.createElement("span", { className: "glyphicon glyphicon-search" })
						)
					),
					React.createElement("input", { type: "search", className: "form-control searchTask", ref: "filter", onChange: onSearch1, placeholder: "Search" })
				)
			),
			React.createElement(
				"div",
				{ className: "col-xs-12" },
				React.createElement(
					"ul",
					{ className: "nav nav-pills todo-filter" },
					React.createElement(
						"li",
						null,
						React.createElement(
							"a",
							{ onClick: onFilter1, className: this.isActive('SHOW_ALL'), value: "SHOW_ALL", href: "#" },
							"All"
						)
					),
					React.createElement(
						"li",
						null,
						React.createElement(
							"a",
							{ onClick: onFilter1, className: this.isActive('false'), value: "false" },
							"Incomplete"
						)
					),
					React.createElement(
						"li",
						null,
						React.createElement(
							"a",
							{ onClick: onFilter1, className: this.isActive('true'), value: "true" },
							"Complete"
						)
					)
				)
			)
		);
	}
});
var ToDoCatalogForm = React.createClass({
	getInitialState: function getInitialState() {
		return { item: '' };
	},
	handleSubmit: function handleSubmit(e) {
		e.preventDefault();
		this.props.onFormSubmit(this.state.item);
		this.setState({ item: '' });
		ReactDOM.findDOMNode(this.refs.item).focus();
		return;
	},
	onChange: function onChange(e) {
		this.setState({
			item: e.target.value
		});
	},
	render: function render() {
		return React.createElement(
			"div",
			{ className: "row" },
			React.createElement(
				"form",
				{ onSubmit: this.handleSubmit },
				React.createElement(
					"div",
					{ className: "form-group " },
					React.createElement(
						"table",
						{ width: "100%" },
						React.createElement(
							"tr",
							null,
							React.createElement(
								"td",
								{ className: "inputAddUserBox" },
								React.createElement("input", { type: "text", placeholder: "Add new user", className: "newTodoCatalogField", ref: "item", onChange: this.onChange, value: this.state.item })
							),
							React.createElement(
								"td",
								null,
								React.createElement("input", { type: "submit", className: "btn btn-default btnAddUser", value: "Add" })
							)
						)
					)
				)
			)
		);
	}
});
var ToDoCatelog = React.createClass({

	changeTodo: function changeTodo(e) {
		this.props.onSelected(e.currentTarget.dataset.id);
	},
	checkActive: function checkActive(i) {

		if (i == this.props.selectedID) {
			return "list-group-item active";
		} else {
			return "list-group-item ";
		}
	},
	render: function render() {

		var selectedID = this.props.selectedID;
		var allitems = this.props.Todos;

		return React.createElement(
			"div",
			{ className: "list-group" },
			allitems.map(function (item, i) {
				var _class = "";
				if (i == this.props.selectedID) {
					_class = "list-group-item active";
				} else {
					_class = "list-group-item ";
				}
				return React.createElement(
					"a",
					{ href: "#", key: i, "data-id": i, className: _class, onClick: this.changeTodo },
					React.createElement(
						"span",
						{ className: "badge" },
						item.items.length
					),
					item.name
				);
			}, this)
		);
	}
});

var TodoApp = React.createClass({
	getInitialState: function getInitialState() {
		return { Todo: [{ name: "Daniel", items: [{ item: 'Todo itme #1', isDone: false }, { item: 'Todo itme #2', isDone: true }, { item: 'aaaa', isDone: true }, { item: 'dddd', isDone: true }] }, { name: "Luis", items: [{ item: 'Todo itme #1', isDone: false }, { item: 'Todo itme #2', isDone: true }, { item: 'Todo itme #3', isDone: true }] }], filter: [{ keyword: '', Status: "SHOW_ALL" }], selectedCatelog: "0" };
	},
	updateItems: function updateItems(newItem) {

		var item = { item: newItem, isDone: false };

		var newtodo = this.state.Todo;
		var allItems = this.state.Todo[this.state.selectedCatelog].items.concat([item]);
		newtodo[this.state.selectedCatelog].items = allItems;
		this.setState({
			Todo: newtodo
		});
	},
	deleteItem: function deleteItem(index) {
		var newtodo = this.state.Todo;
		var allItems = this.state.Todo[this.state.selectedCatelog].items.slice(); //copy array
		allItems.splice(index, 1); //remove element
		newtodo[this.state.selectedCatelog].items = allItems;
		this.setState({
			Todo: newtodo
		});
	},
	filterItem: function filterItem(e) {

		this.state.filter[0].Status = e.target.value;
		this.setState({
			filter: this.state.filter
		});
	},
	searchItem: function searchItem(e) {

		this.state.filter[0].keyword = e.target.value;
		this.setState({
			filter: this.state.filter
		});
	},
	AddCatalog: function AddCatalog(newCatalog) {
		var Catalog = { name: newCatalog, items: [{ item: 'Todo itmd #1', isDone: false }] };
		var newtodo = this.state.Todo.concat([Catalog]);
		this.setState({
			Todo: newtodo
		});
	},
	setSelectedCatalog: function setSelectedCatalog(index) {
		this.state.selectedCatelog = index;
		this.setState({
			selectedCatelog: index
		});
	},
	render: function render() {
		return React.createElement(
			"div",
			{ className: "row" },
			React.createElement(
				"div",
				{ className: "col-xs-4 usersBox" },
				React.createElement(
					"h2",
					null,
					"Users"
				),
				React.createElement(ToDoCatalogForm, { onFormSubmit: this.AddCatalog }),
				React.createElement(ToDoCatelog, { selectedID: this.state.selectedCatelog, onSelected: this.setSelectedCatalog, Todos: this.state.Todo })
			),
			React.createElement(
				"div",
				{ className: "col-xs-8 tasksBox" },
				React.createElement(ToDoBanner, null),
				React.createElement(ToDoFilter, { onFilter: this.filterItem, onSearch: this.searchItem, filter: this.state.filter }),
				React.createElement(ToDoForm, { onFormSubmit: this.updateItems }),
				React.createElement(ToDoList, { items: this.state.Todo[this.state.selectedCatelog].items, filter: this.state.filter, onDelete: this.deleteItem })
			)
		);
	}
});

ReactDOM.render(React.createElement(TodoApp, null), document.getElementById('todo'));